/*
* NOTES:
* const cors = require('cors') 
* 	Allow back-end to be available to front-end app
* 	ALlow us to control the app's Cross Origin Resource Sharing settings
*/


const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors') 
const userRoute = require('./routes/userRoutes')
const port = 4000
const app = express()

//Connect to MongoDB Database
mongoose.connect("mongodb+srv://admin:admin123@course-booking.m222n.mongodb.net/S32-S36?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
})


//Check connection
let db = mongoose.connection
db.on('error', () => console.error.bind(console, 'Connection Error'))
db.once('open', () => console.log('Now connected to MongoDB Atlas'))


app.use(cors()) //ALlows all resources to access the backend app
app.use(express.json()) //Allows read write json format
app.use(express.urlencoded({extended:true})) //Allows read form(body)

//Add the task route
//This will be the default end end point
app.use('/users', userRoute)


app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port: ${process.env.PORT || port}.`)
})