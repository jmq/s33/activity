const jwt = require('jsonwebtoken')
const secret = 'CourseBookingAPI'

//Create jsonWebToken
module.exports.createAccessToken = (user) => { //user variable accepts req.body
	//When the user login, the token will be created 
	//with the user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//Generate JSON web token using sign method
	// sign method - generates the token using form data
	//               and the secret code with 
	//               no additional options provided
	return jwt.sign(data, secret, {})
}